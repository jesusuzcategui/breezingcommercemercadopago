<?php
defined('_JEXEC') or die('Restricted access');
class CrBcInstallation extends CrBcInstaller {
 
 public $type = 'payment';
 public $name = 'mercadopago';

 function install(){
     $query = <<<SQL
     CREATE TABLE #__breezingcommerce_plugin_payment_mercadopago(
        `identity` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
        `canceldir` text NOT NULL,
        `thankdir` text,
        `is_dev` tinyint(4) NOT NULL DEFAULT '0',
        `dev_key` text,
        `dev_token` text,
        `prod_key` text,
        `prod_token` text,
        `binary_mode` int(11) NOT NULL DEFAULT '0' 
     );
     SQL;
     $db = JFactory::getDbo();
     $db->setQuery($query);
     $db->query();
 }

}