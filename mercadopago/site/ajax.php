<?php

define( 'DS', DIRECTORY_SEPARATOR );

if(!defined(JPATH_BASE)){
  define('JPATH_BASE', dirname( dirname( dirname( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) ) ) ));
}

define('_JEXEC', true);

include( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );

require_once( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );
require_once( JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'database' . DS . 'factory.php' );

$mercadopagoDir = JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'mercadopago' . DS . 'vendor' . DS;

require_once $mercadopagoDir . 'autoload.php';

require_once JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'mercadopago' . DS . 'site' . DS . 'countrycurrency.php';

require_once JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'mercadopago' . DS . 'site' . DS . 'mphelper.php';

$response = new stdClass();

JFactory::getApplication('site');

$db = JFactory::getDbo();

$config = JFactory::getConfig();

$mp_session = JFactory::getSession();

$mp_session->start();

$db->setQuery("SELECT * FROM #__breezingcommerce_plugin_payment_mercadopago limit 1");

$mp = $db->loadObject();

MercadoPago\SDK::initialize();

if( intval($mp->is_dev) == 0 ){

  if( trim($mp->prod_token) != "" ) {
    MercadoPago\SDK::setAccessToken( $mp->prod_token );
    $accesso = $mp->prod_token;
  } else {
    throw new Exception('ACCESS TOKEN DONT DEFINED');
  }

} else {

  if( trim($mp->dev_token) != "" ) {
    MercadoPago\SDK::setAccessToken( $mp->dev_token );
    $accesso = $mp->dev_token;
  } else {
    throw new Exception('ACCESS TOKEN DONT DEFINED');
  }

}


switch ($_GET['opt']){

  case 'executePaymentBank':
      header('Content-Type: application/json');

      $preference = new MercadoPago\Preference();
      
      $payment = new MercadoPago\Payment();

      $payer = new MercadoPago\Payer();

      $items = array();

      $payer->email = $_POST['localp_bank_transfer']['email'];

      $payer->entity_type = $_POST['localp_bank_transfer']['typecustomer'];

      $payer->identification = array(
        "type" => $_POST['localp_bank_transfer']['typeid'],
        "number" => $_POST['localp_bank_transfer']['numberid']
      );

      $payment->transaction_amount = round( floatval( $_POST['localp_bank_transfer']['transaction_amount'] ), 2);

      $payment->description = $config->get( 'sitename' );

      $payment->payer = $payer;

      if( intval($_POST['localp_bank_transfer']['binarymode']) == 1){

        $payment->binary_mode = true;

      } else {

        $payment->binary_mode = false;

      }

      $payment->capture = true;

      foreach($_POST['localp_bank_transfer']['items'] as $item){

        $ele = new MercadoPago\Item();

        $ele->id          = intval($item['id']);
        $ele->title       = $item['name'];
        $ele->quantity    = $item['quanty'];
        $ele->unit_price  = countrycurrency::getExchangeSmall( $item['price_net'] )->exchangePrice; 
        $ele->currency_id = countrycurrency::getCurrencyCode();

        array_push($items, $ele);

      }

      $payment->metadata = array(
        "currency_id" => countrycurrency::getCurrencyCode()
      );

      $payment->transaction_details = array(
        "financial_institution" => $_POST['localp_bank_transfer']['bank']
      );

      $payment->additional_info = array(
        "ip_address" => countrycurrency::getIP()
      );

      $payment->callback_url = $_POST['localp_bank_transfer']['returnurl'] . '&mp=1';

      $payment->notification_url = $_POST['localp_bank_transfer']['returnurl'] . '&mp=1';

      $payment->payment_method_id = $_POST['localp_bank_transfer']['methodid'];

      //$payment->shipping_amount = floatval($_POST['localp_bank_transfer']['transaction_shiping']);

      $payment->save();

      //var_dump($payment);

      $mp_session->set('identificadorPago', $payment->id);    

      //print_r($payment);  

      if( $payment->error === NULL){

        $response->error = false;

        $response->urlSubmit = $payment->transaction_details->external_resource_url;

      } else {
        $response->error = true;
        $response->urlSubmit = $payment->transaction_details->external_resource_url;
      }

      echo json_encode($response);

  break;

  case 'executePaymentCred':

    try {


      header('Content-Type: application/json');

      $payment = new MercadoPago\Payment();
      $payer   = new MercadoPago\Payer();

      $items = array();

      foreach($_POST['localp_credit_card']['items'] as $item){

        $ele = new stdClass();

        $ele->id       = intval($item['id']);
        $ele->title    = $item['name'];
        $ele->quantity = $item['quanty'];
        $ele->unit_price = countrycurrency::getExchangeSmall( $item['price_net'] )->exchangePrice; 

        array_push($items, $ele);

      }

      $payment->additional_info = array(
        "items" => $items
      );

      $payment->metadata = array(
        "currency_id" => countrycurrency::getCurrencyCode()
      );

      $payment->transaction_amount = round(floatval( $_POST['localp_credit_card']['transaction_amount'] ), 2);

      $payment->callback_url = $_POST['localp_credit_card']['returnurl'];

      $payment->token = $_POST['localp_credit_card']['token'];
      $payment->description = '';
      $payment->installments = intval($_POST['localp_credit_card']['installments']);
      $payment->payment_method_id = $_POST['localp_credit_card']['methodid'];
      $payer->email = $_POST['localp_credit_card']['email'];
      $payment->payer=$payer;
      $payment->external_reference = $_POST['localp_credit_card']['referencia'];
      //$payment->shipping_amount    = floatval($_POST['localp_credit_card']['transaction_shiping']);

      $payment->save();

      $mp_session->set('identificadorPago', $payment->id);

      if( $payment->error === NULL ){

        $response->error = false;
        $response->urlSubmit = $payment->callback_url . '&mpid=' . $payment->id . '&mperror=0';

      } else {

        $response->error = true;
        $response->urlSubmit = $payment->callback_url . '&mperror=1';
        
      }
      
    } catch (Exception $e) {
      $response->error = true;
      $response->urlSubmit = $payment->callback_url . '&mperror=2';
    }

    echo json_encode($response);

  break;

  case 'procesarpagoCred':

    try {

      $request_body = file_get_contents('php://input');

      $data = json_decode($request_body);

      $urlGeo  = 'http://www.geoplugin.net/php.gp?ip=' . countrycurrency::getIP();

      $ammount = $data->amount;

      if( $ammount != null ){

        $curlGeo = curl_init($urlGeo);

        curl_setopt($curlGeo, CURLOPT_RETURNTRANSFER, true);

        $jsonGeo = curl_exec($curlGeo);

        curl_close($curlGeo);

        $geoplugin = unserialize($jsonGeo);

        $precio    = floatval($geoplugin['geoplugin_currencyConverter']) * $ammount;

        if($data->shipp != null){
          $shipp = countrycurrency::getExchangeAmmount($data->shipp);
        } else {
          $shipp = false;
        }

        $response->exchangeRate     = floatval($geoplugin['geoplugin_currencyConverter']);
        $response->exchangeCurrency = $geoplugin['geoplugin_currencyCode'];
        $response->exchangeCountry  = $geoplugin['geoplugin_countryName'];
        $response->exchangeCity     = $geoplugin['geoplugin_city'];
        $response->exchangePrice    = $precio;
        $response->exchangeAmmount  = $ammount;
        $response->exchangeShipping = ($data->shipp != null) ? $data->shipp : 0;
        $response->shipping         = $shipp;
        $response->error            = false;
        $response->errorMgs         = "";

      } else {

        $response->error            = true;
        $response->errorMgs         = "The ammount send has been error. Please, try again";

      }

    } catch(Exception $e){
      $response->error            = true;
      $response->errorMgs         = $e->getMessage();
    }    

    echo json_encode($response);
        
  break;
}

?>