<?php


class countrycurrency {

  static function getIP(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
  }

  static function getCurrencyCode(){

    $urlGeo  = 'http://www.geoplugin.net/php.gp?ip=' . self::getIP();
    $curlGeo = curl_init($urlGeo);

    curl_setopt($curlGeo, CURLOPT_RETURNTRANSFER, true);

    $jsonGeo = curl_exec($curlGeo);

    curl_close($curlGeo);

    $geoplugin = unserialize($jsonGeo);

    return $geoplugin['geoplugin_currencyCode'];

  }

  static function getCountryName(){

    $urlGeo  = 'http://www.geoplugin.net/php.gp?ip=' . self::getIP();
    $curlGeo = curl_init($urlGeo);

    curl_setopt($curlGeo, CURLOPT_RETURNTRANSFER, true);

    $jsonGeo = curl_exec($curlGeo);

    curl_close($curlGeo);

    $geoplugin = unserialize($jsonGeo);

    return $geoplugin['geoplugin_countryName'];

  }

  static function getExchangeSmall($ammout){

    try {

      $response = new stdClass();

      $urlGeo  = 'http://www.geoplugin.net/php.gp?ip=' . self::getIP();

      if( $ammout != null ){

        $curlGeo = curl_init($urlGeo);

        curl_setopt($curlGeo, CURLOPT_RETURNTRANSFER, true);

        $jsonGeo = curl_exec($curlGeo);

        curl_close($curlGeo);

        $geoplugin = unserialize($jsonGeo);

        $precio    = doubleval($geoplugin['geoplugin_currencyConverter']) * doubleval($ammout);

        $response->exchangeRate     = floatval($geoplugin['geoplugin_currencyConverter']);
        $response->exchangeCurrency = $geoplugin['geoplugin_currencyCode'];
        $response->exchangePrice    = $precio;
        $response->error            = false;
        $response->errorMgs         = "";

      } else {

        $response->error            = true;
        $response->errorMgs         = "The ammount send has been error. Please, try again";

      }

   } catch (Exception $e) {
      $response->error            = true;
      $response->errorMgs         = $e->getMessage();
   } 

   return $response;

  }

  static function getExchangeAmmount($ammount=0){

    $cambio = self::getExchangeSmall($ammount);
    
    if(!$cambio->error){
      return $cambio->exchangePrice;
    } else {
      return null;
    }

  }

  static function getExchange($ammout=null){

   try {

      $response = new stdClass();

      $urlGeo  = 'http://www.geoplugin.net/php.gp?ip=' . self::getIP();

      if( $ammout != null ){

        $curlGeo = curl_init($urlGeo);

        curl_setopt($curlGeo, CURLOPT_RETURNTRANSFER, true);

        $jsonGeo = curl_exec($curlGeo);

        curl_close($curlGeo);

        $geoplugin = unserialize($jsonGeo);

        $precio    = doubleval($geoplugin['geoplugin_currencyConverter']) * doubleval($ammout);

        $response->exchangeRate     = floatval($geoplugin['geoplugin_currencyConverter']);
        $response->exchangeCurrency = $geoplugin['geoplugin_currencyCode'];
        $response->exchangeCountry  = $geoplugin['geoplugin_countryName'];
        $response->exchangeCity     = $geoplugin['geoplugin_city'];
        $response->exchangePrice    = $precio;
        $response->exchangeAmmount  = $ammout;
        $response->error            = false;
        $response->errorMgs         = "";

      } else {

        $response->error            = true;
        $response->errorMgs         = "The ammount send has been error. Please, try again";

      }

   } catch (Exception $e) {
      $response->error            = true;
      $response->errorMgs         = $e->getMessage();
   } 

   return $response;

  }

}


?>