<script>

<?php  if( intval($mp->is_dev) == 0 ): ?>

  <?php if( trim($mp->prod_key) != "" ): ?>
    window.Mercadopago.setPublishableKey("<?php echo $mp->prod_key; ?>");
  <?php else: ?>
    alert('<?php JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_NO_KEY'); ?>');
  <?php endif; ?>

<?php else: ?>

  <?php if( trim($mp->dev_key) != "" ): ?>
    window.Mercadopago.setPublishableKey("<?php echo $mp->dev_key; ?>");
  <?php else: ?>
    alert('<?php JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_NO_KEY'); ?>');
  <?php endif; ?>

<?php endif; ?>

var doSubmit = false;
var nameForm = '';
var idType   = '';


function validateFields(event, message){

  return event.target.setCustomValidity(message);

}


function closeForm(id){

  if(id){
    var rg = document.getElementById('localpayment-render-grid');
    const y = ((rg.getBoundingClientRect().top + window.pageYOffset));
  	window.scrollTo({top: y, behavior: 'smooth'});
    var rf = document.getElementById('localpayment-render-form');
    rf.classList.add('animate__flipOutX');
    var c = document.getElementById(id);
    c.remove();
  }

}


function goPayDo(evento, forma, typeid, ide){

  evento.preventDefault();
  forma.reportValidity();

  switch (typeid) {
    case 'debit_card':
    case 'credit_card':
      if(!doSubmit){
        nameForm = `payment_${ide}`;
        idType = typeid;
        window.Mercadopago.createToken(forma, sdkResponseMethod);
        return false;
      }
      break;
    case 'bank_transfer':

        nameForm = `payment_${ide}`;
        idType = typeid;

        goBankTransfer(forma, typeid);

      break;
  }
}

function goBankTransfer(form, idtype){

  var uri = '<?php echo JURI::base(); ?>media/breezingcommerce/plugins/payment/mercadopago/site/ajax.php?opt=executePaymentBank';

  var boton = form.querySelector('button[type="submit"]');

  boton.innerHTML = `
    
    <span uk-spinner></div>

  `;

  if(form && idtype){

    axios.post(uri, new FormData(form)).then((r)=>{

      var respuesta = r.data;
      if(respuesta.error == false){

        window.location.href=respuesta.urlSubmit;
      } else {
        alert('Has been error');
      }

    }).catch((e)=>{
      console.erro(e);
    });

  } else {
    alert('HAS BEEN ERROR RELOAD PAGE PLEASE');
  }

}

function sdkResponseMethod(status, response){

  var uri = '<?php echo JURI::base(); ?>media/breezingcommerce/plugins/payment/mercadopago/site/ajax.php?opt=executePaymentCred';

  if (status != 200 && status != 201) {
    alert("verify filled data");
  }else{
    console.log(response);
    var form = document.getElementById(nameForm);
    var card = document.createElement('input');
    card.setAttribute('name', `localp_${idType}[token]`);
    card.setAttribute('type', 'hidden');
    card.setAttribute('value', response.id);
    form.appendChild(card);
    //doSubmit=true;
    

    axios.post(uri, new FormData(form)).then(function(done){

      var respuesta = done.data;
      if(respuesta.error == false){

        window.location.href=respuesta.urlSubmit;
      } else {
        alert('Has been error');
      }

    }).catch(function(error){
      console.error(error);
    });
  }
}

function getPaymentMethod(evento, element, targteid){

  var cardnumber = element.value;

  if (cardnumber.length >= 6) {
      let bin = cardnumber.substring(0,6);
      window.Mercadopago.getPaymentMethod({
          "bin": bin,
          "campo_target": targteid
      }, setPaymentMethod);
  }
  
}

function setPaymentMethod(status, response) {
    console.log("RESPONSE: ", response);
    console.log("STATUS: ", status);
    if (status == 200) {

        let paymentMethodId = response[0].id;
        var methodid   = document.querySelector('#localp_methodid');

        //console.log(methodid.value);
        methodid.value = paymentMethodId;

        getInstallments();

    } else {
        alert(`payment method info error: ${response}`);
    }
}

function getInstallments(){
    var methodid   = document.querySelector('#localp_methodid');
    
    var params = {
        "payment_method_id": methodid.value,
        "amount": parseFloat(document.getElementById('localp_transaction_amount').value)
        
    };

    window.Mercadopago.getInstallments( params , function (status, response) {
        console.log(response);
        if (status == 200) {

            var ele = document.querySelector('#localp_installments');
            ele.options.length = 0;

            if(response.length > 0){
                response[0].payer_costs.forEach( installment => {
                    let opt = document.createElement('option');
                    opt.text = installment.recommended_message;
                    opt.value = installment.installments;
                    ele.appendChild(opt);
                });
            } else {
                alert('THIS TRANSACTION DON HAVE INSTALLAMENTS');
            }

            ele.removeAttribute('disabled');
            ele.classList.remove('uk-disabled');
            
        } else {
            alert(`installments method info error: ${response}`);
        }
    });
} 

function getDetailtPayment(monto=null, shipping=null){

	var uri = '<?php echo JURI::base(); ?>media/breezingcommerce/plugins/payment/mercadopago/site/ajax.php?opt=procesarpagoCred';
  document.getElementById('paymentInformation').innerHTML = "";

  axios.post(uri, {
    amount: parseFloat(monto),
    shipp:  parseFloat(shipping)
  }).then((response)=>{
    var alertContainer = document.getElementById('paymentInformation');
    var alertTitle     = document.createElement('h3');
        //alertTitle.classList.add('');
    var alertBody      = document.createElement('p');

    if( response.data.error == false ){

      alertTitle.innerHTML = "<?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_PAYMENT_DATA'); ?>";
      var htmlHY = `<strong><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_AMMOUNT_ORIGIN_STRING'); ?>:</strong> ${response.data.exchangeAmmount} USD<br>`;

      if( (shipping != null) ){
        if(response.data.shipping != false){
          htmlHY += `<strong><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_SHIPPING_ORIGIN_STRING'); ?>: </strong> ${response.data.exchangeShipping} USD <br>`;  
        }
        
      }
          
      htmlHY += `<strong><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_LOCALITY_STRING'); ?>:</strong> ${response.data.exchangeCity} / ${response.data.exchangeCountry}<br/>
                <strong><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_EXCHANGE_STRING'); ?>:</strong> ${response.data.exchangeRate} ${response.data.exchangeCurrency}`;
      
      if( (shipping != null) ){
        if(response.data.shipping != false){
          htmlHY += `<br><strong><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_SHIPPING_STRING'); ?>: </strong> ${response.data.shipping} ${response.data.exchangeCurrency}`;  
        }
        
      }

      htmlHY += `<br><strong><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_AMMOUNT_STRING'); ?>: </strong> ${response.data.exchangePrice} ${response.data.exchangeCurrency}`;

      htmlHY += `<table style="border: solid 1px #ccc; width: 100%;border-collapse: collapse;padding: 30px; margin-top: 15px;">
                  <thead>
                    <tr style="background: #ccc;color: #2d2d2d;padding: 16px;">
                      <td>Item</td>
                      <td>Precio Unitario</td>
                      <td>Cantidad</td>
                    </tr>
                  </thead>
                  <tbody>`;
      
      <?php foreach($mp->items as $in => $it): ?>

        htmlHY += `<tr data-item="<?php echo $it->id; ?>">
                      <td><?php echo $it->title; ?></td>
                      <td><?php echo countrycurrency::getExchangeAmmount($it->price_net); ?> ${response.data.exchangeCurrency}</td>
                      <td><?php echo $it->amount; ?></td>
                   </tr>`;

      <?php endforeach; ?>

      htmlHY += `</tbody>
                </table>`;

      alertBody.innerHTML = htmlHY;

    } else {

      alertTitle.innerHTML = "<?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_ERROR_MESSAGE'); ?>";
      alertBody.innerHTML = `<?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_ERROR_MESSAGE_DETAIL'); ?>`;

    }

    alertContainer.appendChild(alertTitle);
    alertContainer.appendChild(alertBody);
  }).catch((error)=>{
    console.error(error);
  });

}

function convertCurrencyAmmount(monto=null, shipping=null){
  var uri = '<?php echo JURI::base(); ?>media/breezingcommerce/plugins/payment/mercadopago/site/ajax.php?opt=procesarpagoCred';
  document.getElementById('paymentInformation').innerHTML = "";

  axios.post(uri, {
    amount: parseFloat(monto),
    shipp:  parseFloat(shipping)
  }).then((response) => {

    var inputAmount = document.getElementById("localp_transaction_amount");
    var inputShippg = document.getElementById("localp_transaction_shipping");

    if( response.data.error == false ){
      
      inputAmount.value = response.data.exchangePrice;

      if(response.data.shipping != false){
        inputShippg.value = response.data.shipping;
      } else {
        inputShippg.value = 0;
      }

    } else {

    	alert('ERROR');

    }

  }).catch((error)=>{
    console.error(error);
  });
}

/**
 * PROCESAMIENTO DE PAGOS.
 */

 document.addEventListener('DOMContentLoaded', function(){

  var localpayment_render = document.getElementById("localpayment-render");

  var localpayment_render_form = localpayment_render.querySelector("#localpayment-render-form");

  var localpayment_grid   = localpayment_render.querySelector("#localpayment-render-grid");

  var localpayment_item   = localpayment_grid.querySelectorAll('.localepayment__item');

  var Jsonitems           = document.getElementById("jsonitems");

  <?php
  $precio = round( floatval($_order_info->grand_total), 2 );
  $ship_c = round( floatval($_order_info->history_shipping_costs), 2 );
?>

  getDetailtPayment(<?php echo floatval($precio); ?>, <?php echo $ship_c; ?>);

  localpayment_item.forEach(function(a, b){

    a.addEventListener('click', function(event){

      var rg = document.getElementById('localpayment-render-grid');
      rg.style.transtion = "all ease .5s";

      localpayment_render_form.setAttribute('style', 'display: block !important;');
      
      var elementS = localpayment_render_form;

  	const y = elementS.getBoundingClientRect().top + window.pageYOffset;

  	window.scrollTo({top: y, behavior: 'smooth'});

      localpayment_render_form.innerHTML = ``;
      
      switch (a.dataset.paymentType){
        case 'credit_card':
        case 'debit_card':

        var formulario = `<div id="containerForma_${a.dataset.paymentId}">
	        				  <div class="uk-mockup-box animate__animated animate__bounceIn animate__flipOutX_esta animate__delay-1s">
	        					<img class="uk-mockup" src='media/breezingcommerce/plugins/payment/mercadopago/site/tmpl/mockup_tdc.png'>
							  </div>

							  <form onsubmit="javascript:goPayDo(event, this, '${a.dataset.paymentType}', '${a.dataset.paymentId}')" id="payment_${a.dataset.paymentId}" method="post">
							  	<div id="payment_msg" class="localepayment__msg localepayment__hidden"></div>
							  	<div class="uk-width-2-3@l uk-align-center animate__animated animate__fadeInUp animate__fadeOutUp_esta">
							  		<div class="uk-paycontain">
								  		<div class="uk-payform">
								  			<hr>

								  			<div uk-grid>
								  				<div class="uk-width-2-3"><name>Datos Personales</name></div>
								  				<div class="uk-width-1-3">
								  					<div class="uk-text-right">
								  						<a href="javascript:closeForm('containerForma_${a.dataset.paymentId}');" class="localepayment__button-close">
								  							&times;
								  						</a>
								  					</div>
								  				</div>
								  			</div>
								  			
								  			<div class="uk-margin">
								  				<div class="uk-child-width-1-2@l uk-grid-small" uk-grid>
													<div>
														<input oninvalid="validateFields(event, 'Escribe tu nombre como aparece en la tarjeta')" type="text" id="localp_credit_card_fullname" name="localp_credit_card[fullname]" class="" data-checkout="cardholderName" min="3" max="30" placeholder="NOMBRE EN LA TARJETA" pattern="[a-zA-Z\\s]+" required>
													</div>
													<div>
														<input oninvalid="validateFields(event, 'Escribe un email válido')" type="text" id="" name="localp_credit_card[email]" placeholder="EMAIL DEL TITULO" class="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" required>
													</div>
													<div>
														<select  oninvalid="validateFields(event, 'Seleccione un tipo de identificación')" id="" name="localp_credit_card[typeid]" class="" data-checkout="docType" required>
															<option value="">TIPO DE ID</option>
	                                					<?php 
	                                  					foreach($typeId['body'] as $b){
	                                    					echo '<option value="'.$b['id'].'">'.$b['name'].'</option>';
	                                  					}
	                                					?>
	                                					</select>
													</div>
													<div>
														<input oninvalid="validateFields(event, 'Ingrese su número de documento')" type="text" id="" name="localp_credit_card[numberid]" class="" placeholder="NRO ID" data-checkout="docNumber" pattern="[0-9]{2,}" required>
													</div>
												</div>
											</div>

											<div class="uk-width-2-3@l uk-align-center">
												<div class="uk-mockup-live">
													<name1>Tarjetade</name1><name2>Credito</name2>
													<div style="height: 100%; width: 100%">
														<div class="vertical-center">
															<input oninvalid="validateFields(event, 'Ingrese el numero de su tarjeta')" type="text" id="" name="localp_credit_card[numbercard]" class="" placeholder="NUMERO EN LA TARJETA" data-checkout="cardNumber" onkeyup="javascript:getPaymentMethod(event, this)" pattern="[0-9]{6,}" required>
															<div>
																<select id="localp_installments" name="localp_credit_card[installments]" class="uk-select uk-disabled" disabled>
																	<option value="">Cuotas</option>
																</select>
															</div>
															<hr>
															<div class="uk-width-2-3@l uk-width-2-3@m uk-width-1-1@s">
																<tag>Fecha de expiración</tag>
																<div class="uk-child-width-1-2@l uk-grid-collapse" uk-grid>
																	<div>
																		<input data-checkout="cardExpirationMonth" placeholder="MM" type="text" id="" name="localp_credit_card[expmonth]" class="" min="01" max="12" minlength="2"  maxlength="2" pattern="[0-9]{1,2}"  oninvalid="validateFields(event, 'Ingrese el mes de vencimiento de la tarjeta')" pattern="[0-9]{2,}" required>
																	</div>
																	<div>
																		<input data-checkout="cardExpirationYear" placeholder="YYYY" type="text" id="" name="localp_credit_card[expyear]" class="" minlength="4" maxlength="4"   oninvalid="validateFields(event, 'Ingrese el año de vencimiento de la tarjeta')" pattern="[0-9]{4,}" required>
																	</div>
																</div>
															</div>
														</div>

														<div class="vertical-bottom-right">
															<input data-checkout="securityCode" placeholder="CSV" type="text" id="" name="localp_credit_card[csv]" class="" minlength="3" oninvalid="validateFields(event, 'Ingrese el CSV de su tarjeta')" pattern="[0-9]{3,}" required>
														</div>
													</div>
												</div>
											</div>
										</div>
										<button class="uk__payform-button" type="submit">Ejecutar Pago</button>
									</div>
								</div>
								<input type="hidden"  id="localp_methodid" name="localp_credit_card[methodid]"/>
								<input type="hidden"  id="localp_methodid" name="localp_credit_card[returnurl]" value="<?php echo $mp->thankdir; ?>"/>
								<input type="hidden"  id="localp_methodid" name="localp_credit_card[cancelurl]" value="<?php echo $mp->canceldir; ?>"/>
								<input type="hidden" id="localp_referncia" name="localp_credit_card[referencia]" value="<?php echo $mp->order_id; ?>" />
								<input type="hidden" id="localp_binarymode" name="localp_credit_card[binarymode]" value="<?php echo $mp->binary_mode; ?>" />
								<input class="uk-disabled uk-input" value="" id="localp_transaction_amount" type="hidden" name="localp_credit_card[transaction_amount]">
								<input class="uk-disabled uk-input" value="" id="localp_transaction_shipping" type="hidden" name="localp_credit_card[transaction_shiping]">`;

						<?php foreach($mp->items as $in => $it): ?>

			              formulario += `<input type="hidden" name="localp_credit_card[items][<?php echo $in; ?>][id]" value="<?php echo $it->id; ?>">`;
			              formulario += `<input type="hidden" name="localp_credit_card[items][<?php echo $in; ?>][name]" value="<?php echo $it->title; ?>">`;
			              formulario += `<input type="hidden" name="localp_credit_card[items][<?php echo $in; ?>][quanty]" value="<?php echo $it->amount; ?>">`;
			              formulario += `<input type="hidden" name="localp_credit_card[items][<?php echo $in; ?>][price_net]" value="<?php echo countrycurrency::getExchangeAmmount($it->price_net); ?>">`;

			            <?php endforeach; ?>
							formulario +=`</form>
						</div>`;

        setTimeout( () => {
            convertCurrencyAmmount(<?php echo floatval($precio); ?>, <?php echo $ship_c; ?>);
        }, 300 );

          break;

        case 'bank_transfer':

            var formulario = `<div id="containerForma_${a.dataset.paymentId}">
            					<div class="uk-mockup-box animate__animated animate__bounceIn animate__delay-1s">
            						<img class="uk-mockup" src='media/breezingcommerce/plugins/payment/mercadopago/site/tmpl/mockup_transfer.png';>
            					</div>

            					<form onsubmit="javascript:goPayDo(event, this, '${a.dataset.paymentType}', '${a.dataset.paymentId}')" id="payment_${a.dataset.paymentId}" method="post">
	            					<div class="uk-width-2-3@l uk-align-center animate__animated animate__fadeInUp animate__fadeOutUp_esta">

	            						<div class="uk-paycontain">

		            						<div class="uk-payform">
		            							
												<div class="uk-margin-large-top" uk-grid>
									  				<div class="uk-width-2-3"><name>Datos Personales</name></div>
									  				<div class="uk-width-1-3">
									  					<div class="uk-text-right">
									  						<a href="javascript:closeForm('containerForma_${a.dataset.paymentId}');" class="localepayment__button-close">
									  							&times;
									  						</a>
									  					</div>
									  				</div>
									  			</div>

		            							<div class="uk-margin">
		            								<div class="uk-child-width-1-2@l uk-grid-small" uk-grid>
		            									<div>
		            										<select  oninvalid="validateFields(event, 'Debes seleccionar su tipo de entidad')" class="" id="" name="localp_bank_transfer[typecustomer]" required>
						                                        <option value=""> Tipo de persona</option>
						                                        <option value="individual"><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_TYPE_PERSON_1'); ?></option>
						                                        <option value="association"><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_TYPE_PERSON_2'); ?></option>
						                                      </select>
		            									</div>
		            									<div>
		            										<input oninvalid="validateFields(event, 'Ingrese un email valido')" placeholder="Email del titular" type="text" id="" name="localp_bank_transfer[email]" class="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" required>
		            									</div>
		            									<div>
		            										<select id="" name="localp_bank_transfer[typeid]" class="" oninvalid="validateFields(event, 'Debes seleccionar un tipo de identificación')" required>
							                                    <option value="">Tipo de identificación</option>
							                                    <?php 
							                                      foreach($typeId['body'] as $b){
							                                        echo '<option value="'.$b['id'].'">'.$b['name'].'</option>';
							                                      }
							                                    ?>
							                                    </select>
		            									</div>
		            									<div>
		            										<input type="text" id="" name="localp_bank_transfer[numberid]" placeholder="Nro ID" pattern="[0-9]{2,}" oninvalid="validateFields(event, 'Ingrese su número de documento')" required>
		            									</div>
													</div>
												</div>

												<div class="uk-width-2-3@l uk-align-center">
													<div class="uk-mockup-live-bank">
														<name>Entidad Bancaria</name>
														<div>
															<div style="height: 100%; width: 100%">
																<div class="vertical-center">
																	<select id="" name="localp_bank_transfer[bank]" class="" required>
																		<option value="">--SELECT---</option>
									                                    <?php 
									                                      foreach($banks as $bank){
									                                        echo '<option value="'.$bank['id'].'">'.$bank['description'].'</option>';
									                                      }
									                                    ?>
                                    								</select>
																</div>
															</div>
														</div>
													</div>
												</div>

												<button class="uk__payform-button" type="submit">Ejecutar Pago</button>
											</div>

										</div>
									</div>
									<input type="hidden" name="localp_bank_transfer[methodid]" id="localp_bank_transfer_methodid" value="${a.dataset.paymentId}">
									<input type="hidden"  id="localp_methodid" name="localp_bank_transfer[returnurl]" value="<?php echo $mp->thankdir; ?>"/>
                                    <input type="hidden"  id="localp_methodid" name="localp_bank_transfer[cancelurl]" value="<?php echo $mp->canceldir; ?>"/>
                                    <input type="hidden" id="localp_referncia" name="localp_bank_transfer[referencia]" value="<?php echo $mp->order_id; ?>" />
                                    <input class="uk-disabled uk-input" value="" id="localp_transaction_amount" type="hidden" name="localp_bank_transfer[transaction_amount]">
                                    <input class="uk-disabled uk-input" value="" id="localp_transaction_shipping" type="hidden" name="localp_bank_transfer[transaction_shiping]">
                                    <input type="hidden" id="localp_binarymode" name="localp_bank_transfer[binarymode]" value="<?php echo $mp->binary_mode; ?>" />`;

                                    <?php foreach($mp->items as $in => $it): ?>

                        			formulario += `<input type="hidden" name="localp_bank_transfer[items][<?php echo $in; ?>][id]" value="<?php echo $it->id; ?>">`;
                        			formulario += `<input type="hidden" name="localp_bank_transfer[items][<?php echo $in; ?>][name]" value="<?php echo $it->title; ?>">`;
                        			formulario += `<input type="hidden" name="localp_bank_transfer[items][<?php echo $in; ?>][quanty]" value="<?php echo $it->amount; ?>">`;
                        			formulario += `<input type="hidden" name="localp_bank_transfer[items][<?php echo $in; ?>][price_net]" value="<?php echo countrycurrency::getExchangeAmmount($it->price_net); ?>">`;

                      				<?php endforeach; ?>

                         formulario += `
								</form>
							</div>`;

                      setTimeout( () => {
                        convertCurrencyAmmount(<?php echo floatval($precio); ?>, <?php echo $ship_c; ?>);
                      }, 300 );
          
            break;
      }

      var htmlForm = document.createElement('div');
      htmlForm.innerHTML = formulario;

      localpayment_render_form.appendChild(htmlForm);

      

    });

  });

 });

 </script>