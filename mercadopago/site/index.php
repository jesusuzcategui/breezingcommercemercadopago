<?php
/**
 * @package     BreezingCommerce
 * @author      Markus Bopp
 * @link        http://www.crosstec.de
 * @license     GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');

$libpath = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_breezingcommerce' . DS . 'classes' . DS . 'plugin' . DS;

$mercadopagoDir = JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'mercadopago' . DS . 'vendor' . DS;

$mercadopagoDirSite = JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'mercadopago' . DS;

require_once($libpath . 'CrBcAPaymentSitePlugin.php');
require_once($libpath . 'CrBcPaymentSitePlugin.php');
require_once($mercadopagoDir . 'autoload.php');
require_once($mercadopagoDirSite . 'site' . DS . 'countrycurrency.php' );

class CrBc_Plugins_Payment_Mercadopago_Site extends CrBcAPaymentSitePlugin implements CrBcPaymentSitePlugin
{
  
  public function __construct(){
  	parent::__construct();
    $this->table = '#__breezingcommerce_plugin_payment_mercadopago';
  }
  
  public function getPluginIcon(){

    $plugin_path = 'media/breezingcommerce/plugins/payment/mercadopago/';

    $document = JFactory::getDocument();

    $document->addStylesheet( $plugin_path . 'site/node_modules/font-awesome/css/font-awesome.min.css' );

    return '<i class="fa fa-money" aria-hidden="true"></i>';
  }
  
  function getPaymentInfo(){
      
  }
  
  public function getInitOutput(){

    parent::__construct();

    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'CrBcCart.php');

    $_session_cart = JFactory::getSession()->get('crbc_cart', array());

    $db = JFactory::getDbo();

    if(!isset($_session_cart['checkout']) || !isset($_session_cart['checkout']['payment_plugin_id'])){
      throw new Exception('User checkout not performed yet');
    }

    $payment_plugin_id = intval($_session_cart['checkout']['payment_plugin_id']);
        
    $_cart = new CrBcCart( $_session_cart );
    $_cart_items = $_cart->getItems(true);
    
    if(count($_cart_items) == 0){
        
        throw new Exception('Trying to pay an empty cart');
    }

    $db->setQuery("Select * From #__breezingcommerce_plugins Where published = 1 And type = 'shipping' Order By `ordering`");
    $shipping_plugins = $db->loadAssocList();
    
    $data = CrBcCart::getData($_session_cart['order_id'], $_cart_items, -1, -1);
    
    $_order_info = CrBcCart::getOrder(
                                $_session_cart['order_id'], 
                                $_cart, 
                                $_session_cart, 
                                $_cart_items, 
                                $_session_cart['customer_id'],
                                $data,
                                $shipping_plugins,
                                array()
                        );
    
    if($_order_info->grand_total <= 0){
        
        throw new Exception('Trying to use payment local while the total is zero.');
    }

    $dirPath = JPATH_SITE . DS . 'media' . DS . 'breezingcommerce' . DS . 'plugins' . DS . 'payment' . DS . 'mercadopago' . DS;

    $plugin_path = 'media/breezingcommerce/plugins/payment/mercadopago/';

    $document = JFactory::getDocument();

    $document->addStylesheet( $plugin_path . 'site/tmpl/animate.min.css' );
    $document->addStylesheet( $plugin_path . 'site/node_modules/font-awesome/css/font-awesome.min.css' );
    $document->addStylesheet( $plugin_path . 'site/tmpl/style.css' );

    $document->addScript( 'https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js' );
    $document->addScript( $plugin_path . 'site/node_modules/axios/dist/axios.js');

    $db = JFactory::getDbo();

    $db->setQuery("SELECT * FROM ".$this->table." limit 1");

    $mp = $db->loadObject();

    if(!($mp instanceof stdClass)){
      throw new Exception('No Mercado pago settings');
    }

    $mp->items             = $_cart_items;

    $mp->order_id          = $_session_cart['order_id'];

    $mp->no_shipping       = $_cart->isVirtualOrder($_cart_items) ? 1 : 0;

    $mp->shipping          = $_order_info->shipping_costs;

    $mp->payment_plugin_id = $payment_plugin_id;

    if($mp->canceldir == ""){
      $mp->canceldir = JUri::root();
    }

    if($mp->thankdir == ""){
      $mp->thankdir  = JUri::getInstance()->toString();
    }

    $mp->thankdir          = $mp->thankdir . "&verify_payment=1&payment_plugin_id=".$mp->payment_plugin_id."&order_id=".$mp->order_id;

    MercadoPago\SDK::initialize();

    if( intval($mp->is_dev) == 0 ){

      if( trim($mp->prod_token) != "" ) {
        MercadoPago\SDK::setAccessToken( $mp->prod_token );
      } else {
        throw new Exception(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_NO_ACCESSTOKEN'));
      }

    } else {

      if( trim($mp->dev_token) != "" ) {
        MercadoPago\SDK::setAccessToken( $mp->dev_token );
      } else {
        throw new Exception(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_NO_ACCESSTOKEN'));
      }

    }

    //$payment_methods = $this->getPaymentMethods();
    //echo countrycurrency::getCountryName();
    $payment_methods = $this->getPaymentMethods( countrycurrency::getCountryName() );
    $typeId          = $this->getIdentifiesType();
    $banks           = $this->getBanks();

    //$mediosDepago = MercadoPago\SDK::get("/sites/MLC/payment_methods");
    $tiposDocumento = MercadoPago\SDK::get("/sites/MLC/identification_types");

    //echo $this->traslateDetail($pago['body']['status'], $pago['body']['status_detail'], $pago['body']['payment_method_id']);

    ob_start();
    require_once( $dirPath . DS . 'site' . DS . 'main.js.php');    
    require_once( JPATH_SITE . '/media/breezingcommerce/plugins/payment/mercadopago/site/tmpl/payment.php' );
    $content = ob_get_contents();
    ob_end_clean();

    return $content;
  }

  public function getPaymentMethods($country="Colombia"){

  	if($country != "México"){

  		switch ($country) {
  			
  			case 'Argentina':

  				$url = "/sites/MLA/payment_methods";
  				
  				break;

  			case 'Brasil':

  				$url = "/sites/MLB/payment_methods";
  				
  				break;

  			case 'Colombia':

  				$url = "/sites/MCO/payment_methods";
  				
  				break;

  			case 'Chile':

  				$url = "/sites/MLC/payment_methods";
  				
  				break;

  			case 'Uruguay':

  				$url = "/sites/MLU/payment_methods";
  				
  				break;

  			case 'Venezuela':

  				$url = "/sites/MLV/payment_methods";
  				
  				break;

  			case 'Perú':

  				$url = "/sites/MPE/payment_methods";
  				
  				break;
  			
  			default:
  				$url = "/sites/MCO/payment_methods";
  				break;
  		}

  		$payment_methods = \MercadoPago\SDK::get($url);

  	} else {
  		$payment_methods = null;
  	}

    return $payment_methods;
  }
  
  public function verifyPayment(CrBcCart $_cart, stdClass $order){


    $_session_idpago = JFactory::getSession()->get('identificadorPago', null);

    MercadoPago\SDK::initialize();

    $sessiones = JFactory::getSession();

    $db = JFactory::getDbo();

    $db->setQuery("SELECT * FROM ".$this->table." limit 1");

    $mp = $db->loadObject();

    if(!($mp instanceof stdClass)){
      throw new Exception('No Mercado pago settings');
    }

    if( intval($mp->is_dev) == 0 ){

      if( trim($mp->prod_token) != "" ) {
        MercadoPago\SDK::setAccessToken( $mp->prod_token );
      } else {
        throw new Exception(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_NO_ACCESSTOKEN'));
      }

    } else {

      if( trim($mp->dev_token) != "" ) {
        MercadoPago\SDK::setAccessToken( $mp->dev_token );
      } else {
        throw new Exception(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_NO_ACCESSTOKEN'));
      }

    }

    if($_session_idpago == null){

      throw new Exception('Error on process payment');
      

    }

    $pago = MercadoPago\SDK::get("/v1/payments/" . $_session_idpago);

    $status = $pago['body']['status'];
    $detalle = $pago['body']['status_detail'];
    $entidad = $pago['body']['payment_method_id'];
    
    //CAPTURAR ID DE PAGO.
    $mpres = JRequest::getVar('mp', null);

     if( $status == "approved" && $detalle == "accredited" ){

        echo $this->traslateDetail($status, $detalle, $entidad) . '<br>';

        return true;

      } else {

        echo $this->traslateDetail($status, $detalle, $entidad) . '<br>';

        return false;

      }

      return false;

      
  }
  
  public function getPaymentTransactionId(){
      
  }

  function getAfterPaymentInfo(){
    return JText::_('PAYD WITH MERCADO PAGO');
  }

  public function getPluginDisplayName(){    

    return "PAGO LOCAL";
  }

  public function getPluginDescription(){ 

    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'CrBcCart.php');

    $_session_cart = JFactory::getSession()->get('crbc_cart', array());
    $db = JFactory::getDbo();
        
    $_cart = new CrBcCart( $_session_cart );
    $_cart_items = $_cart->getItems(true);
    
    if(count($_cart_items) == 0){
        
        throw new Exception('Trying to pay an empty cart');
    }

    $db->setQuery("Select * From #__breezingcommerce_plugins Where published = 1 And type = 'shipping' Order By `ordering`");

    $shipping_plugins = $db->loadAssocList();
    
    $data = CrBcCart::getData($_session_cart['order_id'], $_cart_items, -1, -1);
    
    $_order_info = CrBcCart::getOrder(
                                $_session_cart['order_id'], 
                                $_cart, 
                                $_session_cart, 
                                $_cart_items, 
                                $_session_cart['customer_id'],
                                $data,
                                $shipping_plugins,
                                array()
                        );

    $exchange = countrycurrency::getExchange($_order_info->grand_total);  

    $montoncito = number_format($exchange->exchangePrice, 2, ',', '.');

    $string = "" .$exchange->exchangeCurrency . " " . $montoncito . " | " . $exchange->exchangeCountry . " | MercadoPago";

    $mens = sprintf($string);

    return $mens;

  }

  public function getIdentifiesType(){
    $types_docs = MercadoPago\SDK::get("/v1/identification_types");
    return $types_docs;
  }

  public function getBanks(){

    $retorno = [];

	$payment_methods = \MercadoPago\SDK::get("/v1/payment_methods");

	foreach($payment_methods['body'] as $m){
		if($m['payment_type_id'] == "bank_transfer"){
			$retorno = $m['financial_institutions'];
		}
    }
    
    return $retorno;  

  }

  public function traslateDetail($status, $details, $payment_id=""){
    if( $status == "approved" ){
  
      switch ($details) {
        case 'accredited':
          return JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_ACCREDITED');
          break;
      }
  
    } else if( $status == "in_process" ) {
  
      switch ($details) {
        case 'pending_contingency':
          return JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_PENDING_CONTINGENGY');
          break;
        case 'pending_review_manual':
          return JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_PENDING_REVIEW_MANUAL');
          break;
      }		
  
    } else {
  
      switch ($details) {
        case 'cc_rejected_bad_filled_card_number':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BAD_FILLED_CARD_NUMBER'), $payment_id );
          break;
  
        case 'cc_rejected_bad_filled_date':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BAD_FILLED_DATE'), $payment_id );
          break;
  
        case 'cc_rejected_bad_filled_other':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BAD_FILLED_OTHER'), $payment_id );
          break;
  
        case 'cc_rejected_bad_filled_security_code':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BAD_FILLED_SECURITY_CODE'), $payment_id );
          break;
        
        case 'cc_rejected_blacklist':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_FILLED_BACKLIST'), $payment_id );
          break;
  
        case 'cc_rejected_call_for_authorize':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_CALL_FOR_AUTHORIZE'), $payment_id );
          break;
  
        case 'cc_rejected_card_disabled':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_CARD_DISABLED'), $payment_id );
          break;
  
        case 'cc_rejected_card_error':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_CARD_ERROR'), $payment_id );
          break;
  
        case 'cc_rejected_duplicated_payment':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_DUPLICATE_PAYMENT'), $payment_id );
          break;
  
        case 'cc_rejected_high_risk':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_HIGH_RISK'), $payment_id );
          break;
  
        case 'cc_rejected_insufficient_amount':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_INSUFFICIENT_AMOUNT'), $payment_id );
          break;
  
        case 'cc_rejected_invalid_installments':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_INVALID_INSTALLMENTS'), $payment_id );
          break;
  
        case 'cc_rejected_max_attempts':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_MAX_ATTEMPTS'), $payment_id );
          break;
  
        case 'cc_rejected_other_reason':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_OTHER_REASON'), $payment_id );
          break;


        case 'bank_error':
          return sprintf( JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BANK_ERROR'), $payment_id );
          break;
        
        default:
          return 'RESPONSE ERROR';
          break;
      }
  
    }
  }
}