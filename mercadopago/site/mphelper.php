<?php


class mphelper
{
    static function traslateRespose($status, $details, $payment_id = "")
    {
        
        if ($status == "approved") {
            
            switch ($details) {
                case 'accredited':
                    return JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_ACCREDITED');
                    break;
            }
            
        } else if ($status == "in_process") {
            
            switch ($details) {
                case 'pending_contingency':
                    return JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_PENDING_CONTINGENGY');
                    break;
                case 'pending_review_manual':
                    return JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_PENDING_REVIEW_MANUAL');
                    break;
            }
            
        } else {
            
            switch ($details) {
                case 'cc_rejected_bad_filled_card_number':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BAD_FILLED_CARD_NUMBER'), $payment_id);
                    break;
                
                case 'cc_rejected_bad_filled_date':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BAD_FILLED_DATE'), $payment_id);
                    break;
                
                case 'cc_rejected_bad_filled_other':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BAD_FILLED_OTHER'), $payment_id);
                    break;
                
                case 'cc_rejected_bad_filled_security_code':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_BAD_FILLED_SECURITY_CODE'), $payment_id);
                    break;
                
                case 'cc_rejected_blacklist':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_FILLED_BACKLIST'), $payment_id);
                    break;
                
                case 'cc_rejected_call_for_authorize':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_CALL_FOR_AUTHORIZE'), $payment_id);
                    break;
                
                case 'cc_rejected_card_disabled':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_CARD_DISABLED'), $payment_id);
                    break;
                
                case 'cc_rejected_card_error':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_CARD_ERROR'), $payment_id);
                    break;
                
                case 'cc_rejected_duplicated_payment':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_DUPLICATE_PAYMENT'), $payment_id);
                    break;
                
                case 'cc_rejected_high_risk':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_HIGH_RISK'), $payment_id);
                    break;
                
                case 'cc_rejected_insufficient_amount':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_INSUFFICIENT_AMOUNT'), $payment_id);
                    break;
                
                case 'cc_rejected_invalid_installments':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_INVALID_INSTALLMENTS'), $payment_id);
                    break;
                
                case 'cc_rejected_max_attempts':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_MAX_ATTEMPTS'), $payment_id);
                    break;
                
                case 'cc_rejected_other_reason':
                    return sprintf(JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_RESPONSE_CC_REJECTED_OTHER_REASON'), $payment_id);
                    break;
                
                default:
                    return 'RESPONSE ERROR';
                    break;
            }
            
        }
        
    }
}


?>