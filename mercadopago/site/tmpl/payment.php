<!--CAMPO DE ELEMENTOS EN EL CARRITO-->

<?php

echo '<input type="text" value="" style="display: none;" name="jsonitems" id="jsonitems">';

    
$JSONitems = json_encode( $mp->items, JSON_UNESCAPED_SLASHES );

?>

<script>

document.addEventListener('DOMContentLoaded', function(){
  var ok = document.getElementById("jsonitems");
  ok.value = JSON.stringify('<?php echo $JSONitems; ?>');
});

</script>

<div id="localpayment-render" class="uk-container">

  <h2 class="uk-text-center"><?php echo JText::_('COM_BREEZINGCOMMERCE_MERCADOPAGO_TITLE_ON_PAYMENT'); ?></h2>


	<div class="uk-width-2-3@l uk-align-center">

	  <div id="paymentInformation" class=""></div>
	  <div id="localpayment-render-grid" class="uk-grid-match uk-child-width-1-3@l uk-grid-small" uk-grid uk-height-match="row: false">

	  <?php if( isset($payment_methods['body']) ): ?>
	    <?php foreach($payment_methods['body'] as $method): ?>

	      <?php if($method['payment_type_id'] !=="ticket" && $method['payment_type_id'] !=="account_money" && $method['payment_type_id'] !=="atm" && $method['payment_type_id'] !=="digital_currency" && $method['payment_type_id'] !=="digital_wallet" && $method['payment_type_id'] !== "prepaid_card"): ?>

	        <div class="animate__animated animate__fadeInUp">
	        <div class="uk-card uk-creditcard">
	        <div class="localepayment__item" style="cursor: pointer;" data-payment-name="<?php echo $method['name']; ?>" data-payment-type="<?php echo $method['payment_type_id']; ?>" data-payment-id="<?php echo $method['id']; ?>" class="localpayment_item" id="localpayment_<?php echo $method['id']; ?>">

	          <div class="">
	            <img src="<?php echo $method['secure_thumbnail']; ?>" class=""/>
	            <hr>
	            <name class=""><?php echo $method['name']; ?></name>
	            <tag class="">Pago Local</tag>
	          </div>

	        </div>
	      	</div>
	      	</div>

	      <?php endif; ?>

	    <?php endforeach; ?>
	  <?php else: ?>
	    <h1>Has been a error</h1>
	  <?php endif; ?>
	  </div>
	</div>

  <div id="localpayment-render-form" class="localepayment__hidden localepayment__card-method">
      
  </div>
</div>


<!-- FORM TDC -->

<!--<div class="uk-mockup-box animate__animated animate__bounceIn animate__flipOutX_esta animate__delay-1s">
	<img class="uk-mockup" src='media/breezingcommerce/plugins/payment/mercadopago/site/tmpl/mockup_tdc.png';>
</div>

<div class="uk-width-2-3@l uk-align-center animate__animated animate__fadeInUp animate__fadeOutUp_esta">

	<div class="uk-payform">

		
		<br><br><br>

		<name>Datos Personales</name>

		<form class="uk-margin">
		<div class="uk-child-width-1-2@l uk-grid-small" uk-grid>
				<div>
					<input placeholder="Nombre en la tarjeta" type="text">
				</div>

				<div>
					<input placeholder="Email del titular" type="text">
				</div>

				<div>
					<input placeholder="Tipo de ID" type="select">
				</div>

				<div>
					<input placeholder="Nro ID" type="number">
				</div>
		</div>
		</form>

		<div class="uk-width-2-3@l uk-align-center">
			<div class="uk-mockup-live">

				<name1>Tarjetade</name1><name2>Credito</name2>

				
				<form>
					<div style="height: 100%; width: 100%">
						<div class="vertical-center">
							<input type="number" name="" placeholder="Número en la Tarjeta">
							<hr>
							<div class="uk-width-2-3@l">
								<tag>Fecha de expiración</tag>

								<div class="uk-child-width-1-2@l uk-grid-collapse" uk-grid>
									<div>
									<input type="number" name="" placeholder="MM">
									</div>
									<div>
									<input type="number" name="" placeholder="YYYY">
									</div>
								</div>
							</div>
						</div>

					<div class="vertical-bottom-right">
						<input type="number" name="" placeholder="CSV">
					</div>

					</div>
				</form>
				

			</div>
		</div>

	<button>Ejecutar Pago</button>

	</div>

</div>

<hr>


FORM TRANSFER

<div class="uk-mockup-box animate__animated animate__bounceIn animate__flipOutX_esta animate__delay-1s">
	<img class="uk-mockup" src='media/breezingcommerce/plugins/payment/mercadopago/site/tmpl/mockup_transfer.png';>
</div>

<div class="uk-width-2-3@l uk-align-center animate__animated animate__fadeInUp animate__fadeOutUp_esta">

	<div class="uk-payform">

		
		<br><br><br>
		<hr>
		<name>Datos Personales</name>

		<form class="uk-margin">
		<div class="uk-child-width-1-2@l uk-grid-small" uk-grid>
				<div>
					<select name="">
						<option>Tipo de persona</option>
					</select>
				</div>

				<div>
					<input placeholder="Email del titular" type="text">
				</div>

				<div>
					<select name="">
						<option>Tipo de ID</option>
					</select>
				</div>

				<div>
					<input placeholder="Nro ID" type="number">
				</div>
		</div>
		</form>

		<div class="uk-width-2-3@l uk-align-center">
			<div class="uk-mockup-live">

				<name>Entidad Bancaria</name>
				
				<form>
					<div style="height: 100%; width: 100%">
						<div class="vertical-center">
							<select name="">
								<option>
									Selecione su banco
								</option>
							</select>
						</div>
					</div>
				</form>

			</div>
		</div>

	<button>Ejecutar Pago</button>

	</div>

</div> -->