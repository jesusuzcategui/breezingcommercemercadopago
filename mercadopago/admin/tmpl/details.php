<?php
/**
 * @package     BreezingCommerce
 * @author      Jesus Uzcategui
 * @link        http://www.crosstec.de
 * @license     GNU/GPL
*/

defined('_JEXEC') or die('Restricted access');
?>

<script type="text/javascript">
    function submitbutton(pressbutton) {
      submitform(pressbutton);
    }

    /**
    * Submit the admin form
    */
    function submitform(pressbutton){
            if (pressbutton) {
                    document.adminForm.task.value=pressbutton;
            }
            if (typeof document.adminForm.onsubmit == "function") {
                    document.adminForm.onsubmit();
            }
            document.adminForm.submit();
    }

    function crbc_submitbutton(pressbutton)
    {
        switch (pressbutton) {
            case 'plugin_cancel':
                pressbutton = 'cancel';
                submitform(pressbutton);
                break;
            case 'plugin_apply':
                var error = false;

                submitform(pressbutton);

                break;
        }
    }

    // Joomla 1.6 compat
    if(typeof Joomla != 'undefined'){
        Joomla.submitbutton = crbc_submitbutton;
    }
    // Joomla 1.5 compat
    submitbutton = crbc_submitbutton;
</script>

<?php

    $options = array(
      'onActive' => 'function(title, description){
          description.setStyle("display", "block");
          title.addClass("open").removeClass("closed");
      }',
      'onBackground' => 'function(title, description){
          description.setStyle("display", "none");
          title.addClass("closed").removeClass("open");
      }',
      'startOffset' => 1,  // 0 starts on the first tab, 1 starts the second, etc...
      'useCookie' => true, // this must not be a string. Don't use quotes.
    );   
    
    $dir_plugin = '../media/breezingcommerce/plugins/payment/mercadopago/admin/tmpl';

  ?>

<div style="display: inline-flex; align-items: center;">
    <img style="width: 100px" src="<?php echo $dir_plugin . DIRECTORY_SEPARATOR . 'mercado-pago-logo.png';?>">
    <h1 style="margin-left: 30px; display: block;">MERCADO PAGO</h1>
</div>

<div class="form-horizontal">

  <div class="control-group">

    <div class="control-label">
      <label for="test_account" class="tip-top hasTooltip" title="<?php echo JHtml::tooltipText( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_TEST_TIP' ); ?>">
      <?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_TEST' ); ?>
      </label>
    </div>

    <div class="controls">
        <label>
          <input type="radio" name="is_dev" id="is_dev_1" value="1" <?php echo ( intval($this->entity->is_dev) == 1) ? 'checked="checked"' : '' ?> /> YES
        </label>

        <label>
          <input type="radio" name="is_dev" id="is_dev_0" value="0" <?php echo ( intval($this->entity->is_dev) == 0) ? 'checked="checked"' : '' ?> /> NOT
        </label>
    </div>

  </div>

<div class="control-group">

  <div class="control-label">
    <label for="test_account" class="tip-top hasTooltip" title="<?php echo JHtml::tooltipText( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_BINARY_TIP' ); ?>">
    <?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_BINARY' ); ?>
    </label>
  </div>

  <div class="controls">
      <label>
        <input type="radio" name="binary_mode" id="binary_mode_1" value="1" <?php echo ( intval($this->entity->binary_mode) == 1) ? 'checked="checked"' : '' ?> /> YES
      </label>

      <label>
        <input type="radio" name="binary_mode" id="binary_mode_0" value="0" <?php echo ( intval($this->entity->binary_mode) == 0) ? 'checked="checked"' : '' ?> /> NOT
      </label>
  </div>

</div>

<?php echo JHtmlTabs::start('tabs_id',$options); ?>


  <?php echo JHtmlTabs::panel("INTEGRATION ON TEST",'panel-id-1'); ?>

  <div class="control-group">

    <div class="control-label">
     <label for="test_account" class="tip-top hasTooltip" title="<?php echo JHtml::tooltipText( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_ACCESS_TOKEN_TIP_TEST' ); ?>"><?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_ACCESS_TOKEN_TEST' ); ?></label>
    </div>

    <div class="controls">
        <input 
          placeholder="<?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_ACCESS_TOKEN_TEST' ); ?>"
          type="text" 
          name="dev_token" 
          id="dev_token" 
          value="<?php echo $this->entity->dev_token; ?>"/>
    </div>

  </div>

  <div class="control-group">

    <div class="control-label">
     <label for="test_account" class="tip-top hasTooltip" title="<?php echo JHtml::tooltipText( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_PUBLISH_KEY_TIP_TEST' ); ?>"><?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_PUBLISH_KEY_TEST' ); ?></label>
    </div>

    <div class="controls">
        <input 
          placeholder="<?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_PUBLISH_KEY_TEST' ); ?>"
          type="text" 
          name="dev_key" 
          id="dev_key" 
          value="<?php echo $this->entity->dev_key; ?>"/>
    </div>

  </div>

  <?php echo JHtmlTabs::panel(JText::_('INTEGRATION ON PRODUCTION'),'panel-id-2'); ?>


  <div class="control-group">

    <div class="control-label">
     <label for="test_account" class="tip-top hasTooltip" title="<?php echo JHtml::tooltipText( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_ACCESS_TOKEN_TIP_PROD' ); ?>"><?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_ACCESS_TOKEN_PROD' ); ?></label>
    </div>

    <div class="controls">
        <input 
          placeholder="<?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_ACCESS_TOKEN_PROD' ); ?>"
          type="text" 
          name="prod_token" 
          id="prod_token" 
          value="<?php echo $this->entity->prod_token; ?>"/>
    </div>

  </div>

  <div class="control-group">

    <div class="control-label">
     <label for="test_account" class="tip-top hasTooltip" title="<?php echo JHtml::tooltipText( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_PUBLISH_KEY_TIP_PROD' ); ?>"><?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_PUBLISH_KEY_PROD' ); ?></label>
    </div>

    <div class="controls">
        <input 
          placeholder="<?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_PUBLISH_KEY_PROD' ); ?>"
          type="text" 
          name="prod_key" 
          id="prod_key" 
          value="<?php echo $this->entity->prod_key; ?>"/>
    </div>

  </div>

  <?php echo JHtmlTabs::panel(JText::_('DATA URL'),'panel-id-3'); ?>


  <div class="control-group">

    <div class="control-label">
     <label for="test_account" class="tip-top hasTooltip" title="<?php echo JHtml::tooltipText( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_URL_CANCEL_TIP' ); ?>"><?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_URL_CANCEL' ); ?></label>
    </div>

    <div class="controls">
        <input 
          placeholder="<?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_URL_CANCEL' ); ?>"
          type="text" 
          name="canceldir" 
          id="canceldir" 
          value="<?php echo $this->entity->canceldir; ?>"/>
    </div>

  </div>

  <div class="control-group">

    <div class="control-label">
     <label for="test_account" class="tip-top hasTooltip" title="<?php echo JHtml::tooltipText( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_URL_THANK_TIP' ); ?>"><?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_URL_THANK' ); ?></label>
    </div>

    <div class="controls">
        <input 
          placeholder="<?php echo JText::_( 'COM_BREEZINGCOMMERCE_MERCADOPAGO_URL_THANK' ); ?>"
          type="text" 
          name="thankdir" 
          id="thankdir" 
          value="<?php echo $this->entity->thankdir; ?>"/>
    </div>

  </div>

  <br>
  <br>



  <?php echo JHtmlTabs::end(); ?>

  <input type="hidden" name="identity" value="<?php echo $this->entity->identity;?>"/>



</div>