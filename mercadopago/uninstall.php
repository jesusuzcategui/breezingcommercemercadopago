<?php

/**
 * @package     BreezingCommerce
 * @author      Jesus Uzcategui
 * @link        http://www.jesusuzcategui.me
 * @license     GNU/GPL
*/


defined('_JEXEC') or die('Restricted access');

class CrBcUninstallation extends CrBcUninstaller {

  public $type = 'payment';
  public $name = 'mercadopago';

  function uninstall(){
      $db = JFactory::getDBO();
      $db->setQuery("DROP TABLE IF EXISTS `#__breezingcommerce_plugin_payment_mercadopago`");
      $db->query();
  }

}